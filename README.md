# Expense Reimbursement System 

## Project 1 Description

This reimbursement system manages the reimbursement processes of employees for any incurred expenses. An employee can login, view their past reimbursements they have submited and submit new reimbursement requests. A financial Manager can login and view all reimbursement requests, approve or deny a request and filter through request via their status. 



## Technologies Used

* Java - version 11
* javax.servlet-api - version 4.0.1
* jackson-databind - version 2.13.2.2
* postgresql - version 42.3.3

## Features

* Sign in as employee or financial manager
* Add reimbursement tickets
* View user's past submitted tickets
* View all employee tickets submitted (as a financial manager)
* Update ticket status (as a financial manager)
* Filter through ticket table (as a financial manager) 

## Getting Started

* To Clone (in git bash): git clone https://gitlab.com/kiiwii98/expense-reimbursement-system.git
* Edit system environment variables for your database
    * New System variables (Variable : Value): 
        * TRAINING_DB_ENDPOINT : your database endpoint 
        * TRAINING_DB_PASSWORD : your database password
        * TRAINING_DB_USERNAME : your database username
* Have Tomcat in order to run program

## Usage

Once an employee is logged in, their past tickets they have submitted show up immediatelty. They are able to submit new tickets by imputting the amount and description of the reimbursement and selecting the type of reimbursemnt (Lodging, Food, Travel, Other).
For the financial manager, all tickets are displayed in a table as soon as they log in. They are able to filter the table view  based on their status (Denied, Approved, Pending). A financial manager can also approve or deny an employee's reimbursement ticket by selecting the ticket's id and whether they approve or deny that ticket.

## License

This project uses the following license: none

