package frontcontroller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name="MasterServlet", urlPatterns= {"/master/*", "/json/*", "/forwarding/*", "/redirecting/*", "/login/*"})
public class MasterServlet extends HttpServlet{

	/*
	 * THE PURPOSE OF THE FRONT CONTROLLER WILL BE TO IMPLEMENT
	 * ANY AUTHENTIFICATION LOGIC YOU MAY HAVE.
	 * 
	 * It modularizes your security.
	 */
	
	protected boolean isLoggedIn(HttpServletRequest req) {

		if ( req.getRequestURI().indexOf("/project1/login/")==-1 &&  req.getSession(false)==null)
			return false;
		else
			return true;
	}
	
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		System.out.println("IN OUR MASTER SERVLET: doGet");
		if(isLoggedIn(req)) {
			
			Dispatcher.myVirtualRouterMethod(req, resp); //HERE I am offloading my work to another entity
		}else {
			resp.getWriter().println("You're not logged in");
		}
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		System.out.println("IN OUR MASTER SERVLET: doPost");

		if(isLoggedIn(req)) {
			
			Dispatcher.myVirtualRouterMethod(req, resp); //HERE I am offloading my work to another entity
		}else {
			resp.getWriter().println("You're not logged in");
		}
	}
}
