package frontcontroller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import controllers.*;

public class Dispatcher {

	/*
	 * THE PURPOSE OF THE DISPATCHER WILL BE TO ACT
	 * AS A VIRTUAL ROUTER, BY PARSING THE URI/URL
	 * AND DETERMINING WHICH CONTROLLER NEEDS TO BE CALLED (Via Switch stmt)
	 */
	
	public static void myVirtualRouterMethod(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
		System.out.println("\n\n\tIN OUR DISPATCHER (myVirtualRouter()) ");

		System.out.println("Current URL: "+req.getRequestURL());
		System.out.println("Current URI: "+req.getRequestURI());
		
		switch(req.getRequestURI()) {
		case "/project1/login/login":
			System.out.println("case 1");
			LoginController.login(req, resp);
			break;	
		case "/project1/forwarding/home":
			System.out.println("case 2");
			String path = "/resources/html/home.html";
			req.getRequestDispatcher(path).forward(req, resp);
			//HomeController.home(req,  resp);
			break;
		case "/project1/forwarding/home2":
			System.out.println("case 3");
			String fPath = "/resources/html/home2.html";
			req.getRequestDispatcher(fPath).forward(req, resp);
			break;
		case "/project1/login/badlogin":
			System.out.println("case 4");
			String bPath = "/resources/html/badlogin.html";
			req.getRequestDispatcher(bPath).forward(req, resp);
			break;
		case "/project1/json/getCurrentUser":
			System.out.println("case 5");
			LoginController.getCurrentUser(req, resp);
			break;
		case "/project1/json/showAllTicks":
			System.out.println("case 6");
			ReimbursementController.showAllTicks(req, resp);
			break;
		case "/project1/json/showPastTicks":
			System.out.println("case 7");
			ReimbursementController.showPastTicks(req, resp);
			break;
		case "/project1/json/addTicket":
			System.out.println("case 8");
			ReimbursementController.addTicket(req, resp);
			break;
		case "/project1/json/updateTicket":
			System.out.println("case 9");
			ReimbursementController.updateTick(req, resp);
			break;
			
		case "/project1/json/viewPending":
			System.out.println("case 10");
			ReimbursementController.showPendingTicks(req, resp);
			break;
		case "/project1/json/viewApproved":
			System.out.println("case 11");
			ReimbursementController.showApprovedTicks(req, resp);
			break;
		case "/project1/json/viewDenied":
			System.out.println("case 12");
			ReimbursementController.showDeniedTicks(req, resp);
			break;
			
		case "/project1/forwarding/logout":
			System.out.println("case 13");
			LogoutController.logOut(req, resp);
			break;
		default:
				System.out.println("In default case: Dude, you gave me a bad URL");

				req.getRequestDispatcher("/resources/html/badlogin.html").forward(req, resp);
		
		}
		
		}
	
}
