package models;

import java.util.List;

public class Users {
	
	private int userId;
	private String username; 
	private String password;
	private String firstName;
	private String lastName;
	private String email;
	private int roleId;   // references userRoles Class (table)
	
	//private List<Reimbursement> reimb; //list of user's reimbursements
////////////CONSTRUCTORS
	
	public Users() {
		
	}
	
	public Users(int userId) {
		this.userId = userId;
	}

	public Users(int userId, String username, String password, String firstName, String lastName, String email,
			int roleId /*, List<Reimbursement> reimb*/) {
		super();
		this.userId = userId;
		this.username = username;
		this.password = password;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.roleId = roleId;
//		this.reimb = reimb;
	}




//////////////GETTERS SETTERS

public int getUserId() {
	return userId;
}


public void setUserId(int userId) {
	this.userId = userId;
}


public String getUsername() {
	return username;
}


public void setUsername(String username) {
	this.username = username;
}


public String getPassword() {
	return password;
}


public void setPassword(String password) {
	this.password = password;
}


public String getFirstName() {
	return firstName;
}


public void setFirstName(String firstName) {
	this.firstName = firstName;
}


public String getLastName() {
	return lastName;
}


public void setLastName(String lastName) {
	this.lastName = lastName;
}


public String getEmail() {
	return email;
}


public void setEmail(String email) {
	this.email = email;
}


public int getRoleId() {
	return roleId;
}


public void setRoleId(int roleId) {
	this.roleId = roleId;
}


//public List<Reimbursement> getReimb() {
//	return reimb;
//}
//
//public void setReimb(List<Reimbursement> reimb) {
//	this.reimb = reimb;
//}

@Override
public String toString() {
	return "Users [userId=" + userId + ", username=" + username + ", password=" + password + ", firstName=" + firstName
			+ ", lastName=" + lastName + ", email=" + email + ", roleId=" + roleId + /*", reimb=" + reimb + */"]";
}

//////////toString


	
	
	
}
