package models;

import java.sql.Timestamp;

public class Reimbursement {

	private int reimbId;
	private double reimbAmt;
	private Timestamp reimbSubmit; //time user submitted reimbursement
	private Timestamp reimbResolved; //time reimbursement approved/denied
	private String reimbDescr; //description of reimbursement 
	private int reimbAuthor; //person filing reimbursement (1 -> employees, 2 -> financial managers)   references userId from Users class
	private int reimbResolver; //person approving/denying reimb ( 2 -> financial managers)             references userId from Users class
	private int reimbStatusId; //1 -> approved, 2 -> denied (?)
	private int reimbTypeId;
	
	
////////////CONSTRUCTORS
	
	public Reimbursement() {
		
	}
	
	public Reimbursement(int reimbId, double reimbAmt, Timestamp reimbSubmit, Timestamp reimbResolved, String reimbDescr,
			 int reimbAuthor, int reimbResolver, int reimbStatusId, int reimbTypeId) {
		super();
		this.reimbId = reimbId;
		this.reimbAmt = reimbAmt;
		this.reimbSubmit = reimbSubmit;
		this.reimbResolved = reimbResolved;
		this.reimbDescr = reimbDescr;
		this.reimbAuthor = reimbAuthor;
		this.reimbResolver = reimbResolver;
		this.reimbStatusId = reimbStatusId;
		this.reimbTypeId = reimbTypeId;
	}

	
////////////GETTERS & SETTERS
	
	public int getReimbId() {
		return reimbId;
	}

	public void setReimbId(int reimbId) {
		this.reimbId = reimbId;
	}

	public double getReimbAmt() {
		return reimbAmt;
	}

	public void setReimbAmt(double reimbAmt) {
		this.reimbAmt = reimbAmt;
	}

	public Timestamp getReimbSubmit() {
		return reimbSubmit;
	}

	public void setReimbSubmit(Timestamp reimbSubmit) {
		this.reimbSubmit = reimbSubmit;
	}

	public Timestamp getReimbResolved() {
		return reimbResolved;
	}

	public void setReimbResolved(Timestamp reimbResolved) {
		this.reimbResolved = reimbResolved;
	}

	public String getReimbDescr() {
		return reimbDescr;
	}

	public void setReimbDescr(String reimbDescr) {
		this.reimbDescr = reimbDescr;
	}

	public int getReimbAuthor() {
		return reimbAuthor;
	}

	public void setReimbAuthor(int reimbAuthor) {
		this.reimbAuthor = reimbAuthor;
	}

	public int getReimbResolver() {
		return reimbResolver;
	}

	public void setReimbResolver(int reimbResolver) {
		this.reimbResolver = reimbResolver;
	}

	public int getReimbStatusId() {
		return reimbStatusId;
	}

	public void setReimbStatusId(int reimbStatusId) {
		this.reimbStatusId = reimbStatusId;
	}

	public int getReimbTypeId() {
		return reimbTypeId;
	}

	public void setReimbTypeId(int reimbTypeId) {
		this.reimbTypeId = reimbTypeId;
	}

	
//////////////toString
	
	@Override
	public String toString() {
		return "Reimbursement [reimbId=" + reimbId + ", reimbAmt=" + reimbAmt + ", reimbSubmit=" + reimbSubmit
				+ ", reimbResolved=" + reimbResolved + ", reimbDescr=" + reimbDescr 
				+ ", reimbAuthor=" + reimbAuthor + ", reimbResolver=" + reimbResolver + ", reimbStatusId="
				+ reimbStatusId + ", reimbTypeId=" + reimbTypeId + "]";
	}
	
	
}
