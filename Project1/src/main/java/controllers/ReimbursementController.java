package controllers;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import models.Reimbursement;
import models.Users;
import services.ReimbursementServices;
import services.ReimbursementServicesImpl;

public class ReimbursementController {

	static ReimbursementServices myReimb = new ReimbursementServicesImpl();
	
	public static void showAllTicks(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{
		List<Reimbursement> allTicks = new ArrayList<>();
		allTicks = myReimb.viewAllTicks();
		System.out.println("LETS GOOO!");
		System.out.println(allTicks);
		
		resp.setContentType("application/json");
		PrintWriter print = resp.getWriter();
		//MyCustomStatusMessage myMessage = new MyCustomStatusMessage("Food Received", "And Contained");
		print.write(new ObjectMapper().writeValueAsString(allTicks));
	}
	
	public static void showPastTicks(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Users user = (Users)req.getSession().getAttribute("currentUser");
		List<Reimbursement> pastTicks = new ArrayList<>();
		pastTicks = myReimb.viewPastTicks(user);
		
		resp.setContentType("application/json");
		PrintWriter print = resp.getWriter();
		print.write(new ObjectMapper().writeValueAsString(pastTicks));
	}
	
	
	public static void showPendingTicks(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		List<Reimbursement> pending = new ArrayList<>();
		pending = myReimb.viewPending(null);
		
		resp.setContentType("application/json");
		PrintWriter print = resp.getWriter();
		print.write(new ObjectMapper().writeValueAsString(pending));
	}
	
	public static void showApprovedTicks(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		List<Reimbursement> approved = new ArrayList<>();
		approved = myReimb.viewApproved(null);
		
		resp.setContentType("application/json");
		PrintWriter print = resp.getWriter();
		print.write(new ObjectMapper().writeValueAsString(approved));
	}
	
	public static void showDeniedTicks(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		List<Reimbursement> denied = new ArrayList<>();
		denied = myReimb.viewDenied(null);
		
		resp.setContentType("application/json");
		PrintWriter print = resp.getWriter();
		print.write(new ObjectMapper().writeValueAsString(denied));
	}
	
	public static void addTicket(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Users current = (Users)req.getSession().getAttribute("currentUser");
		ObjectMapper map = new ObjectMapper();
		
		Reimbursement myR = map.readValue(req.getInputStream(), Reimbursement.class);
		System.out.println(myR);
		myReimb.addReimbursement(myR, current);
	}
	
	public static void updateTick(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Users current = (Users)req.getSession().getAttribute("currentUser");
		ObjectMapper map = new ObjectMapper();
		
		Reimbursement myR = map.readValue(req.getInputStream(), Reimbursement.class);
		System.out.println("updating...");	
		myReimb.updateTick(myR, current);
	}
}
