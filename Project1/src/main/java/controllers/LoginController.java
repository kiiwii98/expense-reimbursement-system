package controllers;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import models.Users;
import services.UsersServices;

public class LoginController {

	public static Users login (HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String myPath = null;

		/*
		 * SPECIFIC route guarding logic could go here
		 * 
		 * Stuff like:
		 * "check if the user has an admin token in their session"
		 * OR
		 * "check if they are using the correct http method"
		 * OR
		 * something to this effect
		 */
		if(!req.getMethod().equals("POST")) {
			myPath = "/index.html";
			req.getRequestDispatcher(myPath).forward(req, resp);
			System.out.println("taking to home pg");
		}
		
		Users user = new Users();
		
		//extract the form data
		user.setUsername(req.getParameter("theirUsername"));
		user.setPassword(req.getParameter("theirPassword"));
		
		
		//go to the DB and get the actual username and password
		

		/*
		 * HERE we are checking to see if the user has the correct username & password
		 * 
		 * IN REALITY, you'll go to the database to get the username & password
		 * you won't hardcode them here. A simple strategy is to use the username
		 * they typed in to go to the database and retrieve the user object for that
		 * given username they typed in; THEN once you have the user object in Java
		 * check to see if the typed in password matches the password from the database.
		 * If they match then log them in, if not tell them they screwed up.
		 * OR
		 * select * from usertable where username=x and password=y;
		 */
		if(UsersServices.verifyLogin(user)) {
			/*
			 * you probably will have a user object that you put into the session
			 * and that user object will contain the username & password. INSTEAD
			 * of putting them in separately
			 */
			
			Users current = UsersServices.storeUser(user);
			
			req.getSession().setAttribute("currentUser", current);
			System.out.println("in login controller this is my user " + current);
			
			///take employee/fin manager to respective home pages
			if (current.getRoleId()==1) {
				myPath = "/forwarding/home";
				} else {
					myPath = "/forwarding/home2";
					System.out.println("financial manager home pg");
				}

			req.getRequestDispatcher(myPath).forward(req, resp);
			
		}else{
			myPath = "/login/incorrectcredentials";
			req.getRequestDispatcher(myPath).forward(req, resp);
		}
		return user; 
	}
	
	public static void getCurrentUser (HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		Users currentUser = (Users)req.getSession().getAttribute("currentUser");
		
		PrintWriter printer = resp.getWriter();
		printer.write(new ObjectMapper().writeValueAsString(currentUser));
		
		
	}

}
