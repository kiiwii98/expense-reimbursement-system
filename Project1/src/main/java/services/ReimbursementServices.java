package services;

import java.util.List;

import models.Reimbursement;
import models.Users;

public interface ReimbursementServices {
	
	public boolean addReimbursement(Reimbursement myReimb, Users user);
	
	public List<Reimbursement> viewPastTicks(Users user);
	
	public List<Reimbursement> viewAllTicks();
	
	public List<Reimbursement> viewByStatus(Reimbursement myReimb);
	
	public List<Reimbursement> viewPending(Reimbursement myReimb);
	
	public List<Reimbursement> viewApproved(Reimbursement myReimb);
	
	public List<Reimbursement> viewDenied(Reimbursement myReimb);
	
	public boolean updateTick(Reimbursement myReimb, Users user);
}
