package services;

import dao.UsersDao;
import dao.UsersDaoImpl;
import models.Users;

public class UsersServicesImpl implements UsersServices{

	static UsersDao uDao = new UsersDaoImpl();
	
	public static boolean verifyLogin(Users user) {
		return uDao.verifyLogin(user);
	}
	
	public static Users storeUser(Users user) {
		return uDao.storeUser(user);
	}
}
