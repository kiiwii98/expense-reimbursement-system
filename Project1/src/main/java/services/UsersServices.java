package services;

import models.Users;

public interface UsersServices {

	public static boolean verifyLogin(Users user) {
		return UsersServicesImpl.verifyLogin(user);
	}
	
	public static Users storeUser(Users user) {
		return UsersServicesImpl.storeUser(user);
	}
	
}
