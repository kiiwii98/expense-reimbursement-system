package services;

import java.util.List;

import dao.ReimbursementDao;
import dao.ReimbursementDaoImpl;
import models.Reimbursement;
import models.Users;

public class ReimbursementServicesImpl implements ReimbursementServices {

	ReimbursementDao rDao = new ReimbursementDaoImpl(); 
	
	@Override
	public boolean addReimbursement(Reimbursement myReimb, Users user) {
		
		return rDao.addReimbursement(myReimb, user);
	}

	@Override
	public List<Reimbursement> viewPastTicks(Users user) {
		
		return rDao.viewPastTicks(user);
	}

	@Override
	public List<Reimbursement> viewAllTicks() {
		
		return rDao.viewAllTicks();
	}

	@Override
	public List<Reimbursement> viewByStatus(Reimbursement myReimb) {
		
		return rDao.viewByStatus(myReimb);
	}

	@Override
	public boolean updateTick(Reimbursement myReimb, Users user) {
		// TODO Auto-generated method stub
		return rDao.updateTick(myReimb, user);
	}

	@Override
	public List<Reimbursement> viewPending(Reimbursement myReimb) {
		
		return rDao.viewPending(myReimb);
	}

	@Override
	public List<Reimbursement> viewApproved(Reimbursement myReimb) {
		
		return rDao.viewApproved(myReimb);
	}

	@Override
	public List<Reimbursement> viewDenied(Reimbursement myReimb) {
		
		return rDao.viewDenied(myReimb);
	}
	
	

}
