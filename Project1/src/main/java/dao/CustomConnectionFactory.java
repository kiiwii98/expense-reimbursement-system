package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/*
 * Ex. of a factory design pattern 
 * 		- A class/object whose purpose 
 * 		  is to generate another class type
 */

public class CustomConnectionFactory {
	
	static { 
        try {
            Class.forName("org.postgresql.Driver");
        }catch(ClassNotFoundException e) {
            e.printStackTrace();
            System.out.println("Static block has failed me");
        }
	}

//	private static String url = "jdbc:postgresql://dbz.ccl4jlqulj9n.us-east-1.rds.amazonaws.com:5432/foodassign";
//	private static String username = "dbz";
//	private static String password = "p4ssw0rd";
	
	public static String url = "jdbc:postgresql://" + System.getenv("TRAINING_DB_ENDPOINT") + "/project1";
	public static String username = System.getenv("TRAINING_DB_USERNAME");
	public static String password = System.getenv("TRAINING_DB_PASSWORD");

	public static Connection getConnection() throws SQLException {
		
		return DriverManager.getConnection(url, username, password);
	}
}
