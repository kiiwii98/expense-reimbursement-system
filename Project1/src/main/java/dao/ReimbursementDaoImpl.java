package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import models.Reimbursement;
import models.Users;

public class ReimbursementDaoImpl implements ReimbursementDao{

	static { 
        try {
            Class.forName("org.postgresql.Driver");
        }catch(ClassNotFoundException e) {
            e.printStackTrace();
            System.out.println("Static block has failed me");
        }
	}
	
	public static void main(String[] args) {
		
		//ReimbursementDao rDao = new ReimbursementDaoImpl();
		
		//System.out.println(rDao.retrieveAllReimbursementsByUserID(2));
	}
	
	@Override
	public boolean addReimbursement(Reimbursement myReimb,Users user) {
		
		try (Connection conn = CustomConnectionFactory.getConnection()) {
			String SQL = "INSERT INTO ERS_REIMBURSEMENT Values(DEFAULT, " + myReimb.getReimbAmt() + ", CURRENT_TIMESTAMP, NULL, '" + myReimb.getReimbDescr() + "'," + user.getRoleId() + ", NULL," + myReimb.getReimbStatusId() + "," + myReimb.getReimbTypeId()+")";
		
			PreparedStatement ps = conn.prepareStatement(SQL);
			
			int numRows = ps.executeUpdate();
			
			if(numRows == 1) {
				System.out.println("tick added");
				return true;
			}
		}catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	
	@Override
	public List<Reimbursement> viewPastTicks(Users user) {
		
		List<Reimbursement> pastTicks = new ArrayList<>();
		
		try (Connection conn = CustomConnectionFactory.getConnection()){
			String SQL = "SELECT * FROM ers_reimbursement WHERE reimb_author = ?";
			
			PreparedStatement ps = conn.prepareStatement(SQL);
			ps.setInt(1, user.getUserId());
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				pastTicks.add(new Reimbursement(rs.getInt("reimb_id"), rs.getInt("reimb_amount"), rs.getTimestamp("reimb_submitted"), 
						rs.getTimestamp("reimb_resolved"), rs.getString("reimb_description"), rs.getInt("reimb_author"),
						rs.getInt("reimb_resolver"), rs.getInt("reimb_status_id"), rs.getInt("reimb_type_id")));
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}		
		return pastTicks;
	}

	
	@Override
	public List<Reimbursement> viewAllTicks() {
		List<Reimbursement> allTicks = new ArrayList<>();
		
		try (Connection conn = CustomConnectionFactory.getConnection()) {
			//go find all restaurants
			String ourSQLStatement = "SELECT * FROM ers_reimbursement";

			PreparedStatement ps = conn.prepareStatement(ourSQLStatement);

			ResultSet rs = ps.executeQuery(); // NOTE.....this is NOT "executeUPdate" it's "executeQuery"

			while (rs.next()) { //let's extract the ResultSet data 
				allTicks.add(new Reimbursement(rs.getInt("reimb_id"), rs.getInt("reimb_amount"), rs.getTimestamp("reimb_submitted"), 
						rs.getTimestamp("reimb_resolved"), rs.getString("reimb_description"), rs.getInt("reimb_author"),
						rs.getInt("reimb_resolver"), rs.getInt("reimb_status_id"), rs.getInt("reimb_type_id")));
			}	
		}catch(SQLException e){
			e.printStackTrace();
		}
		return allTicks;
	}


	@Override
	public List<Reimbursement> viewByStatus(Reimbursement myReimb) {
		List<Reimbursement> statusTicks = new ArrayList<>();
		
		try (Connection conn = CustomConnectionFactory.getConnection()) {
			
			String ourSQLStatement = "SELECT * FROM ers_reimbursement WHERE reimb_status_id = " + myReimb.getReimbStatusId();

			PreparedStatement ps = conn.prepareStatement(ourSQLStatement);

			ResultSet rs = ps.executeQuery(); // NOTE.....this is NOT "executeUPdate" it's "executeQuery"

			while (rs.next()) { //let's extract the ResultSet data 
				statusTicks.add(new Reimbursement(rs.getInt("reimb_id"), rs.getInt("reimb_amount"), rs.getTimestamp("reimb_submitted"), 
						rs.getTimestamp("reimb_resolved"), rs.getString("reimb_description"), rs.getInt("reimb_author"),
						rs.getInt("reimb_resolver"), rs.getInt("reimb_status_id"), rs.getInt("reimb_type_id")));
			}	
		}catch(SQLException e){
			e.printStackTrace();
		}
		
		return statusTicks;
	}


	@Override
	public boolean updateTick(Reimbursement myReimb, Users user) {
		try(Connection conn = CustomConnectionFactory.getConnection()) {

			String SQL = "UPDATE ers_reimbursement SET reimb_resolved = CURRENT_TIMESTAMP, reimb_resolver = " + user.getRoleId() + ", reimb_status_id = " + myReimb.getReimbStatusId() + " WHERE reimb_id =" + myReimb.getReimbId();

			PreparedStatement ps = conn.prepareStatement(SQL);
			int numRow = ps.executeUpdate();
			
			if(numRow == 1) {
				System.out.println("Your ticket was updated");
				return true;
			}
			
		}catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
		return false;
	}



	public List<Reimbursement> viewPending(Reimbursement myReimb){
		
		List<Reimbursement> pending = new ArrayList<>();
		
		try (Connection conn = CustomConnectionFactory.getConnection()) {
			
			String SQL = "SELECT * FROM ers_reimbursement WHERE reimb_status_id = 3";
			PreparedStatement ps =conn.prepareStatement(SQL);
			
			ResultSet rs = ps.executeQuery();
			
			while (rs.next()) { //let's extract the ResultSet data 
				pending.add(new Reimbursement(rs.getInt("reimb_id"), rs.getInt("reimb_amount"), rs.getTimestamp("reimb_submitted"), 
						rs.getTimestamp("reimb_resolved"), rs.getString("reimb_description"), rs.getInt("reimb_author"),
						rs.getInt("reimb_resolver"), rs.getInt("reimb_status_id"), rs.getInt("reimb_type_id")));
			}	
		}catch(SQLException e){
			e.printStackTrace();
		}
		
		return pending;
		}
		
	public List<Reimbursement> viewApproved(Reimbursement myReimb){
		
		List<Reimbursement> approved = new ArrayList<>();
		
		try (Connection conn = CustomConnectionFactory.getConnection()) {
			
			String SQL = "SELECT * FROM ers_reimbursement WHERE reimb_status_id = 1";
			PreparedStatement ps =conn.prepareStatement(SQL);
			
			ResultSet rs = ps.executeQuery();
			
			while (rs.next()) { //let's extract the ResultSet data 
				approved.add(new Reimbursement(rs.getInt("reimb_id"), rs.getInt("reimb_amount"), rs.getTimestamp("reimb_submitted"), 
						rs.getTimestamp("reimb_resolved"), rs.getString("reimb_description"), rs.getInt("reimb_author"),
						rs.getInt("reimb_resolver"), rs.getInt("reimb_status_id"), rs.getInt("reimb_type_id")));
			}	
		}catch(SQLException e){
			e.printStackTrace();
		}
		
		return approved;
		}
	
		public List<Reimbursement> viewDenied(Reimbursement myReimb) {

			List<Reimbursement> denied = new ArrayList<>();

			try (Connection conn = CustomConnectionFactory.getConnection()) {

				String SQL = "SELECT * FROM ers_reimbursement WHERE reimb_status_id = 2";
				PreparedStatement ps = conn.prepareStatement(SQL);

				ResultSet rs = ps.executeQuery();

				while (rs.next()) { // let's extract the ResultSet data
					denied.add(new Reimbursement(rs.getInt("reimb_id"), rs.getInt("reimb_amount"),
							rs.getTimestamp("reimb_submitted"), rs.getTimestamp("reimb_resolved"),
							rs.getString("reimb_description"), rs.getInt("reimb_author"), rs.getInt("reimb_resolver"),
							rs.getInt("reimb_status_id"), rs.getInt("reimb_type_id")));
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}

			return denied;
		}

	}

