package dao;

import java.util.List;

//import java.util.List;

import models.Reimbursement;
import models.Users;

public interface ReimbursementDao {

	//Employees
		//Add Reimbursement Request
	public boolean addReimbursement(Reimbursement myReimb, Users user);
	
		//View Past Tickets
	public List<Reimbursement> viewPastTicks(Users user);
	
	//Financial Manager
	
		//View All Reimbursements For All Employees
	public List<Reimbursement> viewAllTicks();
	
		//Filter request by Status
	public List<Reimbursement> viewByStatus(Reimbursement myReimb);
	
	public List<Reimbursement> viewPending(Reimbursement myReimb);
	
	public List<Reimbursement> viewApproved(Reimbursement myReimb);
	
	public List<Reimbursement> viewDenied(Reimbursement myReimb);
	
		//Approve/Deny Reimbursement
	
	public boolean updateTick(Reimbursement myReimb, Users user);

	 
}
