package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import models.Users;

public class UsersDaoImpl implements UsersDao{

	static { 
        try {
            Class.forName("org.postgresql.Driver");
        }catch(ClassNotFoundException e) {
            e.printStackTrace();
            System.out.println("Static block has failed me");
        }
	}
	
	@Override
	public boolean verifyLogin(Users user) {

		try (Connection conn = CustomConnectionFactory.getConnection()) {

			String ourSQLStatement = "SELECT * FROM ers_users WHERE ers_username = '" + user.getUsername()
				+ "' AND ers_password = '" + user.getPassword() + "'";

			PreparedStatement ps = conn.prepareStatement(ourSQLStatement);
		
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) {
				System.out.println("Logging you in :)");
				Users user2= new Users(rs.getInt("ers_user_id"), rs.getString("ers_username"),rs.getString("ers_password"),
						rs.getString("user_first_name"), rs.getString("user_last_name"), rs.getString("user_email"),
						rs.getInt("user_role_id"));
				System.out.println(user2);
				
				return true;
				
			}
		}
		catch(SQLException e){
			e.printStackTrace();
		}		
		return false;
	}
	
	public Users storeUser(Users user) {
		
		Users user2 = null;
		
		try (Connection conn = CustomConnectionFactory.getConnection()) {

			String ourSQLStatement = "SELECT * FROM ers_users WHERE ers_username = '" + user.getUsername()
				+ "' AND ers_password = '" + user.getPassword() + "'";

			PreparedStatement ps = conn.prepareStatement(ourSQLStatement);
		
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) {
				System.out.println("Logging you in :)");
				user2= new Users(rs.getInt("ers_user_id"), rs.getString("ers_username"),rs.getString("ers_password"),
						rs.getString("user_first_name"), rs.getString("user_last_name"), rs.getString("user_email"),
						rs.getInt("user_role_id"));
				System.out.println(user2);
				
			}
		}
		catch(SQLException e){
			e.printStackTrace();
		}		
		System.out.println(user);
		return user2;
	}

}
