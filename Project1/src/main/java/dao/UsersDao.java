package dao;

import models.Users;

public interface UsersDao {

	public boolean verifyLogin(Users user);
	
	public Users storeUser(Users user);
	
}
