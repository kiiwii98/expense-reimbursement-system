/**
 * 
 */

window.onload = function(){
    console.log("window has loaded!");

   // addTicket();
    //getAllTicks();
    getCurrentUser();
    getPastTicks();
    document.getElementById("addT").addEventListener('click',addTicket);

}

function addTicket(){

    console.log("add ticket button clicked");

    // let textfield = document.getElementById("foodText").value;
    // console.log(textfield);
    let xhttp = new XMLHttpRequest();

    xhttp.onreadystatechange = function () {
        console.log("ready state: ", xhttp.readyState);


        if (xhttp.readyState == 4 && xhttp.status == 200) {
            console.log("All good");
        }
    }

    xhttp.open('POST', 'http://localhost:9003/project1/json/addTicket');
    xhttp.setRequestHeader("content-type", "application/json");

    console.log(document.querySelector('.form-select option:checked').value);

    let type;
    //setting reimbursement type id 
    switch(document.querySelector('.form-select option:checked').value){
        case "1": type = 1;  //lodging
            break;
        case "2": type = 2;  //travel
            break;
        case "3": type = 3;  //food
            break;
        case "4": type = 4;  //other      
            break;
    }
    let sendInput = {
        "reimbAmt": document.getElementById("reimbAmt").value,
        "reimbDescr": document.getElementById("reimbDescr").value,
        "reimbStatusId": 3,
        "reimbTypeId": type
    }   
    console.log(sendInput);
    /////this is to send input, turn it to a JSON, and send it
    xhttp.send(JSON.stringify(sendInput));
};


function getCurrentUser(){
    let xmlhttp = new XMLHttpRequest();

    console.log("getting current user");

    xmlhttp.onreadystatechange = function (){
        console.log("inside on ready state changefunction")

        if(xmlhttp.readyState == 4 && xmlhttp.status == 200){
            console.log("state is 4 status 200");
            let currentUser = JSON.parse(xmlhttp.responseText);
            console.log(currentUser);
            DOMCurrentUser(currentUser);
        };
    };
    
    xmlhttp.open('POST', 'http://localhost:9003/project1/json/getCurrentUser');
    xmlhttp.send();
};

function DOMCurrentUser(currentUser){
    document.getElementById("user").innerText = currentUser.firstName;

}


function getPastTicks(){
    let xmlhttp = new XMLHttpRequest();

    console.log("made it to getAllTicks()");

    xmlhttp.onreadystatechange = function (){
        console.log("inside on ready state changefunction")

        if(xmlhttp.readyState == 4 && xmlhttp.status == 200){
            console.log("state is 4 status 200");
            let tickObject = JSON.parse(xmlhttp.responseText);
            console.log(tickObject);

             DOMManipulation(tickObject);
           
        };
    };
    
    xmlhttp.open('POST', 'http://localhost:9003/project1/json/showPastTicks');
    xmlhttp.send();
};

function DOMManipulation(tickObject){
    for(let i = 0; i < tickObject.length; i++){
        ////////////CREATE ELEMEENTS DYNAMICALLY////////////////

        //step 1: creaitng our new elements
        let newTR = document.createElement("tr");  //represents row
        let newTH = document.createElement("th");  //represents header

        let newTD1 = document.createElement("td");  //data
        let newTD2 = document.createElement("td");
        let newTD3 = document.createElement("td");
        let newTD4 = document.createElement("td");
        let newTD5 = document.createElement("td");
        let newTD6 = document.createElement("td");
        let newTD7 = document.createElement("td");
        let newTD8 = document.createElement("td");
        let newTD9 = document.createElement("td");

        //step 2: populate our creations
        newTH.setAttribute("scope", "row");
        let myTextH = document.createTextNode(tickObject[i].reimbId);
        let myTextD1 = document.createTextNode(tickObject[i].reimbAmt);
        let myTextD2 = document.createTextNode(tickObject[i].reimbSubmit);
        let myTextD3 = document.createTextNode(tickObject[i].reimbResolved);
        let myTextD4 = document.createTextNode(tickObject[i].reimbDescr);
        let myTextD5 = document.createTextNode(tickObject[i].reimbAuthor);
        let myTextD6 = document.createTextNode(tickObject[i].reimbResolver);
        let myTextD7 = document.createTextNode(tickObject[i].reimbStatusId);
        let myTextD8 = document.createTextNode(tickObject[i].reimbTypeId);
        
        //all appending
        newTH.appendChild(myTextH);
        newTD1.appendChild(myTextD1);
        newTD2.appendChild(myTextD2);
        newTD3.appendChild(myTextD3);
        newTD4.appendChild(myTextD4);
        newTD5.appendChild(myTextD5);
        newTD6.appendChild(myTextD6);
        newTD7.appendChild(myTextD7);
        newTD8.appendChild(myTextD8);

        newTR.appendChild(newTH);
        newTR.appendChild(newTD1);
        newTR.appendChild(newTD2);
        newTR.appendChild(newTD3);
        newTR.appendChild(newTD4);
        newTR.appendChild(newTD5);
        newTR.appendChild(newTD6);
        newTR.appendChild(newTD7);
        newTR.appendChild(newTD8);
        newTR.appendChild(newTD9);

        let newSelection = document.querySelector("#tickTableBody");
        newSelection.appendChild(newTR);
    }
}
