/**
 * 
 */

window.onload = function() {
    console.log("Window for FM has loaded!")

    getCurrentUser();
    getAllTicks();

    document.getElementById("update").addEventListener('click', updateTicket);

    document.getElementById("pend").addEventListener('click',getPending);
    document.getElementById("deny").addEventListener('click',getDenied);
    document.getElementById("approve").addEventListener('click', getApproved);
}

function getPending(){
    let xmlhttp = new XMLHttpRequest();
    console.log("want to show pending tickets");

    xmlhttp.onreadystatechange = function(){
        console.log("inside on ready state changefunction");

        if(xmlhttp.readyState == 4 && xmlhttp.status == 200){
            console.log("state is 4 status 200");
            let pendTicks = JSON.parse(xmlhttp.responseText);
            console.log(pendTicks);
            DOMManipulation(pendTicks);
        }
    }
    xmlhttp.open('GET', 'http://localhost:9003/project1/json/viewPending');
    xmlhttp.send();
}

function getApproved(){
    let xmlhttp = new XMLHttpRequest();
    console.log("want to show approved tickets");

    xmlhttp.onreadystatechange = function(){
        console.log("inside on ready state changefunction");

        if(xmlhttp.readyState == 4 && xmlhttp.status == 200){
            console.log("state is 4 status 200");
            let approvedTicks = JSON.parse(xmlhttp.responseText);
            console.log(approvedTicks);
            DOMManipulation(approvedTicks);
        }
    }
    xmlhttp.open('GET', 'http://localhost:9003/project1/json/viewApproved');
    xmlhttp.send();
}

function getDenied(){
    let xmlhttp = new XMLHttpRequest();
    console.log("want to show denied tickets");

    xmlhttp.onreadystatechange = function(){
        console.log("inside on ready state changefunction");

        if(xmlhttp.readyState == 4 && xmlhttp.status == 200){
            console.log("state is 4 status 200");
            let deniedTicks = JSON.parse(xmlhttp.responseText);
            console.log(deniedTicks);
            DOMManipulation(deniedTicks);
        }
    }
    xmlhttp.open('GET', 'http://localhost:9003/project1/json/viewDenied');
    xmlhttp.send();
}

function getCurrentUser(){
    let xmlhttp = new XMLHttpRequest();

    console.log("getting current user");

    xmlhttp.onreadystatechange = function (){
        console.log("inside on ready state changefunction")

        if(xmlhttp.readyState == 4 && xmlhttp.status == 200){
            console.log("state is 4 status 200");
            let currentUser = JSON.parse(xmlhttp.responseText);
            console.log(currentUser);
            DOMCurrentUser(currentUser);
        };
    };
    
    xmlhttp.open('POST', 'http://localhost:9003/project1/json/getCurrentUser');
    xmlhttp.send();
};

function DOMCurrentUser(currentUser){
    document.getElementById("user").innerText = currentUser.firstName;

}


function getAllTicks(){
    let xmlhttp = new XMLHttpRequest();

    console.log("made it to getAllTicks()");

    xmlhttp.onreadystatechange = function (){
        console.log("inside on ready state changefunction")

        if(xmlhttp.readyState == 4 && xmlhttp.status == 200){
            console.log("state is 4 status 200");
            let tickObject = JSON.parse(xmlhttp.responseText);
            console.log(tickObject);

             DOMManipulation(tickObject);
           
        };
    };
    
    xmlhttp.open('POST', 'http://localhost:9003/project1/json/showAllTicks');
    xmlhttp.send();
};

function DOMManipulation(tickObject){

    let tableSelection = document.querySelector("#tickTableBody");

    while(tableSelection.firstChild){

        tableSelection.removeChild(tableSelection.firstChild);
    }

    for(let i = 0; i < tickObject.length; i++){
        ////////////CREATE ELEMEENTS DYNAMICALLY////////////////

        //step 1: creaitng our new elements
        let newTR = document.createElement("tr");  //represents row
        let newTH = document.createElement("th");  //represents header

        let newTD1 = document.createElement("td");  //data
        let newTD2 = document.createElement("td");
        let newTD3 = document.createElement("td");
        let newTD4 = document.createElement("td");
        let newTD5 = document.createElement("td");
        let newTD6 = document.createElement("td");
        let newTD7 = document.createElement("td");
        let newTD8 = document.createElement("td");
        let newTD9 = document.createElement("td");

        //step 2: populate our creations
        newTH.setAttribute("scope", "row");
        let myTextH = document.createTextNode(tickObject[i].reimbId);
        let myTextD1 = document.createTextNode(tickObject[i].reimbAmt);
        let myTextD2 = document.createTextNode(tickObject[i].reimbSubmit);
        let myTextD3 = document.createTextNode(tickObject[i].reimbResolved);
        let myTextD4 = document.createTextNode(tickObject[i].reimbDescr);
        let myTextD5 = document.createTextNode(tickObject[i].reimbAuthor);
        let myTextD6 = document.createTextNode(tickObject[i].reimbResolver);
        let myTextD7 = document.createTextNode(tickObject[i].reimbStatusId);
        let myTextD8 = document.createTextNode(tickObject[i].reimbTypeId);
        
        //all appending
        newTH.appendChild(myTextH);
        newTD1.appendChild(myTextD1);
        newTD2.appendChild(myTextD2);
        newTD3.appendChild(myTextD3);
        newTD4.appendChild(myTextD4);
        newTD5.appendChild(myTextD5);
        newTD6.appendChild(myTextD6);
        newTD7.appendChild(myTextD7);
        newTD8.appendChild(myTextD8);

        newTR.appendChild(newTH);
        newTR.appendChild(newTD1);
        newTR.appendChild(newTD2);
        newTR.appendChild(newTD3);
        newTR.appendChild(newTD4);
        newTR.appendChild(newTD5);
        newTR.appendChild(newTD6);
        newTR.appendChild(newTD7);
        newTR.appendChild(newTD8);
        newTR.appendChild(newTD9);

        let newSelection = document.querySelector("#tickTableBody");
        newSelection.appendChild(newTR);
    }
}


function updateTicket() {
    console.log("Want to update a ticket");

    let xhttp = new XMLHttpRequest();

    xhttp.onreadystatechange = function(){
    //     let target = JSON.parse(xhttp.responseText);
    //     console.log(target);
    //     DOMManipulation(target);
    }

    xhttp.open('POST', 'http://localhost:9003/project1/json/updateTicket')
    xhttp.setRequestHeader("content-type", "application/json");

    let status;

    switch(document.querySelector('.form-select option:checked').value){
        case "Approve": status = 1;
            break;
        case "Deny": status = 2;
            break;
    }

    let updatedT = {
        "reimbId": document.getElementById("RId").value,
        "reimbStatusId": status 
    }
    console.log(updatedT);
    xhttp.send(JSON.stringify(updatedT));
}




